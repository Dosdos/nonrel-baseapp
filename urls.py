import dbindexer
from django.conf.urls import patterns, include
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin


handler500 = 'djangotoolbox.errorviews.server_error'

# django admin
admin.autodiscover()
# search for dbindexes.py in all INSTALLED_APPS and load them
dbindexer.autodiscover()


urlpatterns = patterns('',
      
    # django
    ('^_ah/warmup$', 'djangoappengine.views.warmup'),
    ('^admin/', include(admin.site.urls)),

    # Auth: log in / log out
    (r'^login/$', 'views.login_user'),
    (r'^logout/$', 'views.logout_user', {}, 'logout'),

)

urlpatterns += i18n_patterns('',

    # testapp
    (r'^testapp/', include('testapp.urls')),

)
