from django.db import models

from utils.debug_manager import custom_logger


def remove_files_on_model_update(sender, instance, **kwargs):
    if not instance.pk:
        return

    try:
        old_instance = instance.__class__.objects.get(pk=instance.pk)
    except instance.DoesNotExist:
        return

    for field in instance._meta.fields:
        if not isinstance(field, models.FileField):
            continue
        old_file = getattr(old_instance, field.name)
        new_file = getattr(instance, field.name)
        storage = old_file.storage
        if old_file and old_file != new_file and storage and storage.exists(old_file.name):
            try:
                storage.delete(old_file.name)
            except Exception:
                custom_logger("Unexpected exception while attempting to delete old file", {'old_file.name': old_file.name, })


def remove_files_on_model_deletion(sender, instance, **kwargs):
    for field in instance._meta.fields:
        if not isinstance(field, models.FileField):
            continue
        file_to_delete = getattr(instance, field.name)
        storage = file_to_delete.storage
        if file_to_delete and storage and storage.exists(file_to_delete.name):
            try:
                storage.delete(file_to_delete.name)
            except Exception:
                custom_logger("Unexpected exception while attempting to delete old file", {'old_file.name': file_to_delete.name, })
