# coding: utf-8

from django import template
from django.core.urlresolvers import reverse
from settings import MEDIA_PREFIX


register = template.Library()


def media_prefix():
    """
    Returns the string contained in the setting MEDIA_PREFIX.
    
    insert the following in the template: 
    {% load utils_tags %}{% media_prefix %}
    """
    return MEDIA_PREFIX


media_prefix = register.simple_tag(media_prefix)

