#############################
#** BRANCH CONFIGURATIONS **#
#############################


# Configurations for 'LOCAL' branch
LOCAL_CONF = {
	# Appengine configurations
	'APPSPOT_NAME': 'nonrel-baseapp',
	'APPSPOT_ABSOLUTE_URL': 'https://127.0.0.1:8000',

	# Cookie age
	'SESSION_COOKIE_AGE': 60*60*24*30*2,   # about 2 months, in seconds

	'CSRF_SECURE': False, # False disables csrf control when developing in local

	'DEBUG': True,

	'ALLOWED_HOSTS': ['127.0.0.1', 'localhost'],
}


# Configurations for 'DEVELOPMENT' branch
DEV_CONF = {
	# Appengine configurations
	'APPSPOT_NAME': 'nonrel-baseapp',
	'APPSPOT_ABSOLUTE_URL': 'https://nonrel-baseapp.appspot.com',

	# Cookie age
	'SESSION_COOKIE_AGE': 60*60*24,   # a day in seconds

	'CSRF_SECURE': True,

	'DEBUG': True,

	'ALLOWED_HOSTS': ['nonrel-baseapp.appspot.com'],
}


# Configurations for 'PRODUCTION' branch
PROD_CONF = {
	# Appengine configurations
	'APPSPOT_NAME': 'nonrel-baseapp',
	'APPSPOT_ABSOLUTE_URL': 'https://nonrel-baseapp.appspot.com',

	# Cookie age
	'SESSION_COOKIE_AGE': 60*60*24,   # a day in seconds

	'CSRF_SECURE': True,

	'DEBUG': False,

	'ALLOWED_HOSTS': ['nonrel-baseapp.appspot.com'],
}


# Configurations dicts mapping
CONF_DICT = {
	'local': LOCAL_CONF,
	'dev': DEV_CONF,
	'prod': PROD_CONF,
	}


# Edit dict key to select the current branch
CONF = CONF_DICT['prod']
