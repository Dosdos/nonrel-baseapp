from django import forms
from users.models import UserProfile

class UserProfileCreationForm(forms.ModelForm):
	class Meta:
		model = UserProfile
		fields = (
			'accepted_eula',
			'favorite_animal',
			)