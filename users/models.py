from django.contrib.auth.models import User
from django.db import models


class UserProfile(models.Model):
    # This field is required.
    user = models.OneToOneField(User)

    # Other fields here
    accepted_eula = models.BooleanField(default=False)
    favorite_animal = models.CharField(max_length=20, default="Dragons.")

    def __unicode__(self):
        return u'%s' % self.user.username
