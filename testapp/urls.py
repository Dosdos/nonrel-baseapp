from django.conf.urls import patterns, url, include

urlpatterns = patterns('testapp.views',
                       
    (r'^$', 'home', {'template_name': 'testapp/home.html'}, 'testapp_home' ),
    (r'^create_random_article[/]?$', 'create_random_article', {'template_name': 'testapp/home.html'}, 'create_random_article' ),
    (r'^login_required[/]?$', 'login_required', {'template_name': 'testapp/login_required.html'}, 'login_required' ),
    (r'^signup[/]?$', 'signup', {'template_name': 'auth/signup.html'}, 'signup' ),

    # Example Models
    (r'^example_models[/]?$', 'example_models', {'template_name': 'testapp/example_model/example_models.html'}, 'example_models'),
    (r'^example_model_create[/]?$', 'example_model_create', {'template_name': 'testapp/example_model/example_model_create.html'}, 'example_model_create'),
    (r'^example_model/(?P<example_model_slug>[-\w]+)[/]?$', 'example_model', {'template_name': 'testapp/example_model/example_model.html'}, 'example_model'),
    (r'^example_model/(?P<example_model_slug>[-\w]+)/edit[/]?$', 'example_model_edit', {'template_name': 'testapp/example_model/example_model_edit.html'}, 'example_model_edit'),
    (r'^example_model/(?P<example_model_slug>[-\w]+)/delete[/]?$', 'example_model_delete', {}, 'example_model_delete'),



    # Change language: en, it
    (r'^en/$', 'change_lang_en', {}, 'change_to_en'),
    (r'^it/$', 'change_lang_it', {}, 'change_to_it'),

)

