from django import forms
from django.conf import settings
from testapp.models import ExampleModel


class ExampleModelForm(forms.ModelForm):
    class Meta:
        model = ExampleModel

        fields = (
            'name',
            'description',
            'is_public',
        )

        widgets = {
            'name': forms.TextInput(attrs={'class': "form-control", 'placeholder': "Name", }),
            'description': forms.Textarea(attrs={'class': "form-control", 'placeholder': "Description", }),
            'is_public': forms.CheckboxInput(attrs={'type':"checkbox", 'class':"uniform", }) ,
        }
