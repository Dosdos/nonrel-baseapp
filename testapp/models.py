from django.db import models
from utils.slugify import unique_slugify


class Article(models.Model):
    """
    docstring for Article
    """

    title = models.CharField(max_length=30, )
    subtitle = models.CharField(max_length=50, )
    author = models.CharField(max_length=50, )
    content = models.CharField(max_length=5000, )
    created = models.DateTimeField(auto_now_add=True, )
    published = models.BooleanField(default=False, )


class ExampleModel(models.Model):
    name = models.CharField(max_length=30, )
    slug = models.SlugField(max_length=255, unique=True, )
    description = models.TextField()
    is_public = models.BooleanField(default=False, )

    def save(self):
        unique_slugify(self, self.name)
        super(ExampleModel, self).save()

    def __unicode__(self):
        return u'%s' % self.name