# Create your views here.
from django.shortcuts import render_to_response
from django.contrib.auth import authenticate, login, REDIRECT_FIELD_NAME, logout as auth_logout
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.template.response import TemplateResponse
from django.utils.http import is_safe_url
from django.utils.translation import ugettext as _
from django.contrib.sites.models import get_current_site


def login_user(request, template_name="auth/login.html"):
    """ Logs in the user asking for 'username' and 'password'.
    
    """
    state = "Devi autenticarti ..."
    username = password = ''
    if request.POST:
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                state = "Ti sei autenticato con successo!"
                if request.GET.get('next', False):
                    return HttpResponseRedirect(request.GET.get('next'))
                else:
                    return HttpResponseRedirect('/testapp/')
            else:
                state = "Il tuo account non risulta attivo, contatta l'amministratore del sito."
        else:
            state = "Username e/o password non corretti."

    return render_to_response(template_name, {'state': state, 'username': username},
                              context_instance=RequestContext(request))


def logout_user(request, next_page=None,
                template_name='auth/logout.html',
                redirect_field_name=REDIRECT_FIELD_NAME,
                current_app=None, extra_context=None):
    """ Logs out the user and displays 'You are logged out' message.

    """
    auth_logout(request)

    if redirect_field_name in request.REQUEST:
        next_page = request.REQUEST[redirect_field_name]
        # Security check -- don't allow redirection to a different host.
        if not is_safe_url(url=next_page, host=request.get_host()):
            next_page = request.path

    if next_page:
        # Redirect to this page until the session has been cleared.
        return HttpResponseRedirect(next_page)

    current_site = get_current_site(request)
    context = {
        'site': current_site,
        'site_name': current_site.name,
        'title': _('Logged out')
    }
    if extra_context is not None:
        context.update(extra_context)
    return TemplateResponse(request, template_name, context,
                            current_app=current_app)
