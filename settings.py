import os

from conf import *


LANGUAGE_CODE = 'en'

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'Europe/Rome'

AUTHENTICATION_BACKENDS = (
    # A Django authentication backend that supports Django's user and group permissions on Django-Nonrel
    'permission_backend_nonrel.backends.NonrelPermissionBackend',
)

INSTALLED_APPS = (

    # django
    'django.contrib.admin',
    'django.contrib.contenttypes',
    'django.contrib.auth',

    'django.contrib.sessions',

    # django-nonrel
    'autoload',
    'dbindexer',
    'djangotoolbox',
    'permission_backend_nonrel',
    # 'filetransfers',
    'djangoappengine',  # djangoappengine should come last, so it can override a few manage.py commands

    # utils and packages
    'utils',
    'bootstrap_django_tags',

    # apps
    'testapp',
    'users',

)


# Original testapp configuration
# DATABASES['native'] = DATABASES['default']
# DATABASES['default'] = {'ENGINE': 'dbindexer', 'TARGET': 'native'}

# Activate django-dbindexer for the default database
DATABASES = {
    'default': {
        'ENGINE': 'dbindexer',
        'TARGET': 'gae',
    },
    'gae': {
        'ENGINE': 'djangoappengine.db',
    },
}

MIDDLEWARE_CLASSES = (
    # This loads the index definitions, so it has to come first
    'autoload.middleware.AutoloadMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.request',
)

DBINDEXER_BACKENDS = (
    'dbindexer.backends.BaseResolver',
    'dbindexer.backends.InMemoryJOINResolver',
)

# This test runner captures stdout and associates tracebacks with their
# corresponding output. Helps a lot with print-debugging.
TEST_RUNNER = 'djangotoolbox.test.CapturingTestSuiteRunner'

AUTOLOAD_SITECONF = 'indexes'

DEBUG = CONF['DEBUG']


# #########################
# Session configurations #
##########################
SESSION_ENGINE = 'django.contrib.sessions.backends.signed_cookies'
SESSION_COOKIE_HTTPONLY = True
SESSION_COOKIE_SECURE = CONF['CSRF_SECURE']
SESSION_COOKIE_NAME = 'scn'
SESSION_COOKIE_AGE = CONF['SESSION_COOKIE_AGE']  # 1 hour, in seconds
CSRF_COOKIE_NAME = 'ccn'
CSRF_COOKIE_SECURE = CONF['CSRF_SECURE']  # False along with SESSION_COOKIE_SECURE to disable csrf control when developing in local
SECRET_KEY = 't=mk2shglm-858&9t0jzl5vc9q%u7xpxv$o-m$q3m53=qpsdfa'


#############################
# Addressing configurations #
#############################
ALLOWED_HOSTS = CONF['ALLOWED_HOSTS']
STATIC_URL = '/static/'
MEDIA_PREFIX = '/media/'
MEDIA_ROOT = os.path.join(os.path.dirname(__file__), 'media')
TEMPLATE_DIRS = (os.path.join(os.path.dirname(__file__), 'templates'),)
ROOT_URLCONF = 'urls'
AUTH_PROFILE_MODULE = 'users.UserProfile'


#################
# i18n and i10n #
#################
USE_I18N = True
